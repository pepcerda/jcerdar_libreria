def call() {
    def abortPipeline = false
    timeout(time: 5, unit: 'MINUTES') {
        waitForQualityGate abortPipeline: true
    }

    return abortPipeline
}